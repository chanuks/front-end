import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import TopNav from './Components/TopNav';
import LandingPage from './Modules/LandingPage/LandingPage';
import SignUp from './Modules/SignUp/SignUp'
import SignUpModal from './Modules/SignUp/SignUpModal';
import LoginModal from './Modules/Login/LoginModal';
import NutritionalAssessment from './Modules/NutritionalAssessment/NutritionalAssessment';
import UserHome from './Modules/UserHome/UserHome';
import UserPreference from './Modules/UserPreference/UserPreference';
import multi from './Modules/UserPreference/multi';
import MealPlan from './Modules/MealPlan/MealPlan';
import NewUserHome from './Modules/NewUserHome/NewUserHome';
import Profile from './Modules/Profile/Profile';

class App extends Component {


  constructor(props){
    super(props);
   
  }

  componentDidMount(){
    
  }
  render() {

      return (
     <div>
      
     


        <Router>
   
        <div>
        <TopNav/>
        
          <hr />
          <Switch>
              <Route path='/home' component={LandingPage} /> 
              <Route path='/userhome' component={UserHome} /> 
              <Route path='/newuserhome' component={NewUserHome} /> 
              <Route path='/signupmodal' component={SignUpModal} />
              <Route path='/login' component={LoginModal} />
              <Route path='/nutriassess' component={NutritionalAssessment}/>
              <Route path='/userpreference' component={UserPreference}/>
              <Route path='/mealPlan' component={MealPlan}/>
              <Route path='/multi' component={multi}/>
              <Route path='/profile' component={Profile}/>
           
              
          </Switch>
        </div>
      </Router>

      </div>
      )
   
    
  }
}

export default App