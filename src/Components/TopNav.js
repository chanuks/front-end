import React from 'react';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button
} from 'reactstrap';

import { Link, withRouter } from 'react-router-dom';
import SignUpModal from '../Modules/SignUp/SignUpModal';
import LoginModal from '../Modules/Login/LoginModal';
import './TopNav.css'




class TopNav extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      username:null
    };
  }

  componentDidMount(){
    // let user =localStorage.getItem("userInfo");
    // if(user){
    //   this.userInformation = JSON.parse(localStorage.getItem('userInfo'));
    //   console.log("User", this.userInformation.username)

    //   this.setState({ username: this.userInformation.username})
    // }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  logout=()=>{
    localStorage.removeItem("userInfo")
    this.props.history.push("/home")
  }

  home=()=>{
    if(localStorage.getItem("userInfo")){
      this.props.history.push("/userhome")
    }else{
      this.props.history.push("/home")
    }
  }

  profile=()=>{
    this.props.history.push("/profile")
  }

  render() {
    // const VehicleSelections = (
    //   if(){}
    //   this.state.vehicleTypeData.map((item) => {
    //   return( 
    //   <DropdownItem key={item.detailKey}>
    //     <Link to={{pathname:  + item.detailKey}}>
    //       {item.vehicleType}
    //     </Link>
    //   </DropdownItem>
    //   )
    // }, this));

    return (<div>
      <Navbar color="dark" dark expand="lg" classname="navbar navbar-expand-lg navbar-dark bg-dark">
        <NavbarBrand class="navbar-brand mb-0 h1" href="/">NutriPulz</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem >
              <NavLink onClick={this.home}>Home </NavLink>
            </NavItem>
            {localStorage.getItem("userInfo")?
            <NavItem >
              <NavLink onClick={this.profile}>Profile </NavLink>
            </NavItem>
            :null}

            </Nav>
            <Nav className="ml-auto" navbar>
              {localStorage.userInfo===undefined?
              <>
            <NavItem>
              <SignUpModal outline color="success" buttonLabel="Register" />
            </NavItem>
            <NavItem>
              <LoginModal outline color="success" buttonLabel="Login" />
            </NavItem>
            </>
            :
            <>
            <NavItem>
              <h4 className="text-white" >Welcome, {JSON.parse(localStorage.getItem('userInfo')).username}</h4>
            </NavItem>
            <NavItem>
              <Button onClick={this.logout}>Logout</Button>
            </NavItem>
            </>
            }

            </Nav>
        </Collapse>
      </Navbar>
    </div>);
  }
}

export default withRouter (TopNav);