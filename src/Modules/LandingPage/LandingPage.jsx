import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'

import { Jumbotron, Button } from 'reactstrap';
import './LandingPage.css'
import SignUpModal from '../SignUp/SignUpModal';



class LandingPage extends Component {

  constructor(props){
    super(props);
    this.state={
      open:false
    }
  }

  openModal=()=>{
    this.setState({
      open:true
    })
  }

  closeModal=()=>{
    this.setState({
      open:false
    })
  }

  render() {
    return (

      <div>


        <Jumbotron  >
          <h1 className="display-3">Welcome to NutriPulz</h1>
          <h4 className="lead">Find out about your nutritional state by taking our nutritional assesmment..</h4>
          <hr className="my-2" />
          
          <p className="lead">
            {/* <Button href='/nutriassess' color="success">Register Here <img src="https://img.icons8.com/color/48/000000/health-checkup.png"/></Button> */}
            <Button onClick={this.openModal} color="success">Register Here</Button>
          </p>


        </Jumbotron>
        <Carousel />

        <SignUpModal open={this.state.open} close={this.closeModal} outline color="success" buttonLabel="Register" />

      </div>

    )
  }
}

export default LandingPage 