import React, { Component } from 'react';
import { useHistory } from "react-router-dom";
import { withRouter } from "react-router-dom";
import {
  Container, Col, Form,
  FormGroup, Label, Input,
  Button, FormText, FormFeedback,
} from 'reactstrap';
import './Login.css';
 import AuthService from '../../Service/AuthService'

class Login extends Component {
  
  constructor(props) {
    super(props);
      this.state = {
      username: '',
      password: '',
      validate: {
        emailState: '',
      },
    }
    this.handleChange = this.handleChange.bind(this);
  }

  validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const { validate } = this.state
      if (emailRex.test(e.target.value)) {
        validate.emailState = 'has-success'
      } else {
        validate.emailState = 'has-danger'
      }
      this.setState({ validate })
    }

  handleChange = async (event) => {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [ name ]: value,
    });
  }



  submitForm = (e) => {
    
 
          const credentials = {username: this.state.username, password: this.state.password};
          AuthService.login(credentials).then(res => {
            console.log('test cred',res);
              if(res.status === 200){
                  localStorage.setItem("userInfo", JSON.stringify(res.data));
                  console.log('test cred',res);
                  this.props.closeModal()
                  this.props.history.push('/userhome')
                  
              }else {
                  this.setState({message: res.data.message});
              }
          });
      };





  render() {
    const { username, password } = this.state;
    
    return (
 
 
      <div class="row justify-content-center">
     
        
        <Form className="form" onSubmit={ (e) => this.submitForm(e) }>
        
          <Col >
            <FormGroup>
              <Label>Username</Label>
              <Input
                type="text"
                name="username"
                id="exampleEmail"
              
                 value={ username }
                // valid={ this.state.validate.emailState === 'has-success' }
                // invalid={ this.state.validate.emailState === 'has-danger' }
                onChange={ (e) => {
                            this.validateEmail(e)
                            this.handleChange(e)
                          } }
              />
          
            </FormGroup>
          </Col>
          <Col >
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input
                type="password"
                name="password"
                id="examplePassword"
               
                value={ password }
                onChange={ (e) => this.handleChange(e) }
            />
            </FormGroup>
          </Col>
         <Col><Button onClick={() => this.submitForm()}>Submit</Button></Col>
          
          
         
      </Form>
      </div>

     
    );
  }
}


export default withRouter(Login);