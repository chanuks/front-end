import React, { Component } from 'react'
import { Spinner, Card, Row, CardTitle, CardSubtitle, Badge, CardBody, Container, CardHeader, Button, Jumbotron } from 'reactstrap';
import {capture, OutputType} from "html-screen-capture-js"
import JSZip from 'jszip'


export class MealPlan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            plan: true
        }
    }

    componentDidMount() {
        if (this.props.location.state) {
            console.log(this.props.location.state.data)
            this.setState({ data: this.props.location.state.data })
            setTimeout(() => {
                this.getMealPlan()
            }, 2000)
        }
    }

    getMealPlan = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        fetch('http://localhost:8080/getMeal', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({


                userId: this.state.data.userId,
                der: this.state.data.der,
                breakfastFood: this.state.data.breakfastFood,
                lunchFood: this.state.data.lunchFood,
                dinnerFood: this.state.data.dinnerFood,
                eatingPattern: this.state.data.eatingPattern,
                userAllergies: this.state.data.userAllergies,
                userMedicalConditions: this.state.data.userMedicalConditions
                // userId: this.userInformation.userId



            })
        })
            .then(response => response.json())
            .then(item => {
                this.setState({
                    plan: item,
                    loading: false
                })
            })
            .catch(err => console.log(err))
    }

    regen=async()=>{
       await this.setState({loading:true})
       this.getMealPlan()
    }

    saveHtml=async ()=>{
        // const cap =capture();
        const htmlDocstr = capture(OutputType.STRING, window.document);

        const jsZip = new JSZip();
        jsZip.file("test.html", htmlDocstr);
        const screenCaptureZipFile = await jsZip.generateAsync({type: 'blob', compression: 'DEFLATE'});
        const screenCaptureZipFileBase64 = await this.convertBlobToBase64(screenCaptureZipFile);

        // console.log("HTML", screenCaptureZipFileBase64)

        // window.location.href = "data:application/octet-stream;base64," + screenCaptureZipFile;
        

    }

    convertBlobToBase64=(blob)=>{
        var reader = new FileReader();
        reader.readAsDataURL(blob); 
        reader.onloadend = function() {
            var base64data = reader.result;                
            console.log(base64data);
                // const linkSource = `data:application/zip;base64,${base64data}`;
                const linkSource = `${base64data}`;
            const downloadLink = document.createElement("a");
            const fileName = "Meal Plan.zip";

            downloadLink.href = linkSource;
            downloadLink.download = fileName;
            downloadLink.click();
                }  
        
    }


    render() {
        return (
            <div>

                <Container>

                    <Button color="success" onClick={this.regen}>Re-Generate Meal</Button>
                    <Button onClick={this.saveHtml}>Save Meal Plan</Button>
                </Container>
                {this.state.loading === true ?
                    <Spinner style={{ width: '8rem', height: '8rem', marginLeft: '45%', marginTop: '10%' }} type="grow" />
                    :


                    <Container>


                        <div>
                            <Jumbotron>
                                <h1 className="display-3">Meal Plan</h1>
                <p className="lead">Required Nutrient Amount for your meal : <b>Carbs - </b>  {Math.trunc(this.state.plan.requiredCHO)}g |  <b>Protein - </b> {Math.trunc(this.state.plan.requiredProtein)}g |  <b>Fat - </b> {Math.trunc(this.state.plan.requiredFat)}g</p>
                                <hr className="my-2" />
                                <p className="lead">Amount of nutrient in the following meal : <b>Carbs- </b>  {Math.trunc(this.state.plan.generatedCHO)}g |  <b>Protein - </b> {Math.trunc(this.state.plan.generatedProtein)}g |  <b>Fat - </b> {Math.trunc(this.state.plan.generatedFat)}g</p>
                              
                            </Jumbotron>
                        </div>

                        <Row style={{ paddingLeft: '3vh' }}>
                            <h3>Breakfast </h3>
                        </Row>
                        <Row style={{ paddingLeft: '3vh' }}>
                            {this.state.plan.breakfastFoods.breakfastFoods.map((item, index) => {
                                return (
                                    <Card style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                        <CardHeader tag="h5">{item.food}</CardHeader>
                                        <CardBody>

                                            <CardSubtitle>{item.foodGroup}</CardSubtitle>
                                            <h5><Badge>{item.servingsAmount} {item.unit}</Badge></h5>
                                        </CardBody>

                                    </Card>

                                )
                            })}
                        </Row>
                        <hr className="my-2" />
                        <Row style={{ paddingLeft: '3vh' }}>
                            <h3>Snack 1</h3>
                        </Row>
                        <Row style={{ paddingLeft: '3vh' }}>

                            {this.state.plan.snack1Foods.snack1Foods.map((item, index) => {
                                return (
                                    <Card style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                        <CardHeader tag="h5">{item.food}</CardHeader>
                                        <CardBody>

                                            <CardSubtitle>{item.foodGroup}</CardSubtitle>
                                            <h5><Badge>{item.servingsAmount} {item.unit}</Badge></h5>
                                        </CardBody>

                                    </Card>

                                )
                            })}</Row>
                        <hr className="my-2" />

                        <Row style={{ paddingLeft: '3vh' }}>
                            <h3>Lunch </h3>
                        </Row>
                        <Row style={{ paddingLeft: '3vh' }}>

                            {this.state.plan.lunchFoods.lunchFoods.map((item, index) => {
                                return (
                                    <Card style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                        <CardHeader tag="h5">{item.food}</CardHeader>
                                        <CardBody>

                                            <CardSubtitle>{item.foodGroup}</CardSubtitle>
                                            <h5><Badge>{item.servingsAmount} {item.unit}</Badge></h5>
                                        </CardBody>
                                    </Card>

                                )
                            })}</Row>
                        <hr className="my-2" />
                        <Row style={{ paddingLeft: '3vh' }}>
                            <h3>Snack 2</h3>
                        </Row>
                        <Row style={{ paddingLeft: '3vh' }}>

                            {this.state.plan.snack2Foods.snack2Foods.map((item, index) => {
                                return (
                                    <Card style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                        <CardHeader tag="h5">{item.food}</CardHeader>
                                        <CardBody>

                                            <CardSubtitle>{item.foodGroup}</CardSubtitle>
                                            <h5><Badge>{item.servingsAmount} {item.unit}</Badge></h5>
                                        </CardBody>

                                    </Card>

                                )
                            })}</Row>
                        <hr className="my-2" />
                        <Row style={{ paddingLeft: '3vh' }}>
                            <h3>Dinner </h3>
                        </Row>
                        <Row style={{ paddingLeft: '3vh' }}>

                            {this.state.plan.dinnerFoods.dinnerFoods.map((item, index) => {
                                return (
                                    <Card style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                        <CardHeader tag="h5">{item.food}</CardHeader>
                                        <CardBody>

                                            <CardSubtitle>{item.foodGroup}</CardSubtitle>
                                            <h5><Badge>{item.servingsAmount} {item.unit}</Badge></h5>
                                        </CardBody>
                                    </Card>

                                )
                            })}</Row>
                        <hr className="my-2" />
                    </Container>
                }

                
            </div>
        )
    }
}

export default MealPlan