import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'

import { Jumbotron, Button, Container, Row, Col, Spinner, Table } from 'reactstrap';




class NewUserHome extends Component {
    userInformation;
    state = {
      
        username:'',
        latest:null
    }

    componentDidMount(){
        // this.userInformation = JSON.parse(localStorage.getItem('userInfo'));
        // console.log("User", this.userInformation.username)
        // this.setState({ username: this.userInformation.username})
   
    }

 
    

  render() {

    
    return (

      <div>


        <Jumbotron>
          {/* <Container> */}
            <Row>
              <Col>
                <>
                <h1 className="display-3">Take your nutritional assessment here...</h1>
                <h4 className="lead">Find out about your nutritional state by taking our nutritional assesmment..</h4>
                <hr className="my-2" />
                
                <p className="lead">
                  <Button href='/nutriassess' color="success">Nutritional Assessment <img src="https://img.icons8.com/color/48/000000/health-checkup.png"/></Button>
                </p>          
                </>
              </Col>
            
            </Row>
          {/* </Container> */}
        </Jumbotron>
        <Carousel />



      </div>

    )
  }

}
export default NewUserHome