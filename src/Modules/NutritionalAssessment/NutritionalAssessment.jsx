import React, { Component, useState } from 'react'
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css'
import Select from 'react-select';
import './NutritionalAssesment.css'
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import moment from "moment";

import { Jumbotron, Col, Row, Button, Form, FormGroup, Label, Input, Container, Table } from 'reactstrap';
import { timers } from 'jquery';



const options = [
    { value: '1.40', label: 'Extremely Inactive' },
    { value: '1.69', label: 'Sedentary' },
    { value: '1.99', label: 'Moderately active' },
    { value: '2.40', label: 'Vigorously active' },
    { value: '3.40', label: 'Extremely active' },
];

const optionsGender = [
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },

];






class NutritionalAssessment extends Component {
    userInformation;
    constructor(props) {
        super(props);

        this.state = { value: 10 };
    }


    state = {
        selectedOption: null,
        selectedOptionGender: null,
        height: 0,
        weight: 0,
        waist: null,
        hip: null,
        bmi: null,
        bmr: null,
        gender: '',
        ibw: null,
        DER: null,
        PAL: null,
        riskFactors: '',
        date: new Date(),
        open: false,
    };

    // componentDidMount(){

    // }


    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    handleChange = selectedOption => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption.value);
        this.setState({ PAL: selectedOption.value })

        console.log(`PAL:` + this.state.PAL);
    };
    handleChangeGender = selectedOptionGender => {
        this.setState({ selectedOptionGender });
        console.log(`Option selected:`, selectedOptionGender.value);
    };
    onChange = e => {

        this.setState({ [e.target.name]: e.target.value })

    }

    calBMI = () => {
        this.setState({ bmi: (this.state.weight / Math.pow((this.state.height / 100), 2)) })
        console.log("bmi:" + this.state.bmi);

    }

    calIBW = () => {

        this.setState({ ibw: (23 * (Math.pow((this.state.height / 100), 2))) })

        console.log("ibw: " + this.state.ibw);
        console.log("gender: " + this.state.gender);
    }

    calBMR = () => {



        if (this.state.gender == "Male") {
            this.setState({ bmr: (1 * Math.trunc(this.state.ibw) * 24) })


        }
        if (this.state.gender == "Female") {
            this.setState({ bmr: (0.9 * Math.trunc(this.state.ibw) * 24) })


        }


    }

    calDER = () => {
        this.setState({ DER: this.state.bmr * this.state.PAL })

        localStorage.setItem("der", JSON.stringify(this.state.DER));

        console.log("der" + this.state.DER);
    }


    getBmi = (bmi) => {
        if (bmi < 18.5) {
            return <div class="p-3 mb-2 bg-danger text-white">Under Weight</div>;
        }
        if (bmi >= 18.5 && bmi < 22.9) {
            return <div class="p-3 mb-2 bg-success text-white">Normal Weight</div>;
        }
        if (bmi >= 23 && bmi < 24.9) {
            return <div class="p-3 mb-2 bg-warning text-dark">Over Weight</div>;
        }
        if (bmi >= 25 && bmi < 30) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 1</div>;
        }
        if (bmi >= 30 && bmi < 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 2</div>;
        }
        if (bmi >= 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 3</div>;
        }
    }


    getWaist = (waist, gender) => {
        if (gender == "Male" && waist >= 90) {
            return <div class="p-3 mb-2 bg-danger text-white">Abdominal Obesity</div>;
        }
        if (gender == "Female" && waist >= 80) {
            return <div class="p-3 mb-2 bg-danger text-white">Abdominal Obesity</div>;
        }

    }

    getIBW = () => {
        return "The healthy body weight based on your Age,Gender & Height"
    }



    // submitFormAdd = e => {
    //     this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


    //     console.log("User", this.userInformation.userId)
    //     console.log("User gender", this.userInformation.gender)

    //     fetch('http://localhost:8080/nutripulz/nutritionalScreenings', {
    //         method: 'post',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({

    //             height: this.state.height,
    //             weight: this.state.weight,
    //             waist: this.state.waist,
    //             hip: this.state.hip,
    //             bmr: Math.trunc(this.state.bmr),
    //             bmi: this.state.bmi,
    //             ibw: this.state.ibw,
    //             der: this.state.DER,
    //             date: moment().format(),
    //             riskFactors: this.state.riskFactors,
    //             user:"http://localhost:8080/nutripulz/users/" +this.userInformation.userId,



    //         })
    //     })
    //         .then(response => response.json())
    //         .then(item => {
    //             if (Array.isArray(item)) {
    //                 this.props.addItemToState(item[0])
    //                 this.props.toggle()

    //             } else {
    //                 console.log('failure')
    //             }
    //         })
    //         .catch(err => console.log(err))
    // }


    saveNutritionalScreening = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        fetch('http://localhost:8080/nutripulz/nutritionalScreenings', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                height: Math.trunc(this.state.height),
                weight: Math.trunc(this.state.weight),
                waist: Math.trunc(this.state.waist),
                hip: Math.trunc(this.state.hip),
                pal: Math.trunc(this.state.PAL),
                bmi: Math.trunc(this.state.bmi),
                ibw: Math.trunc(this.state.ibw),
                bmr: Math.trunc(this.state.bmr),
                der: Math.trunc(this.state.DER),
                gender: this.state.gender,
                date: new Date(),
                user: "http://localhost:8080/nutripulz/users/" + this.userInformation.userId,



            })
        })
            .then(response => response.json())
            .then(item => {
                if (Array.isArray(item)) {
                    this.props.addItemToState(item[0])
                    this.props.toggle()

                } else {
                    console.log('failure')
                }
                this.setState({
                    open: false
                })
            })
            .catch(err => console.log(err))
    }





    submitFormAdd = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        fetch('http://localhost:8080/nutripulz/userInfoes', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                der: "2000",
                breakfastFood: "cereals",
                lunchFood: "rice",
                dinnerFood: "bread",
                eatingPattern: "anything",
                userAllergies: ["treenuts", "pineapple"],
                userMedicalConditions: ["diabetes", "hypertension"],
                user: "http://localhost:8080/nutripulz/users/" + this.userInformation.userId,



            })
        })
            .then(response => response.json())
            .then(item => {
                if (Array.isArray(item)) {
                    this.props.addItemToState(item[0])
                    this.props.toggle()

                } else {
                    console.log('failure')
                }
            })
            .catch(err => console.log(err))
    }

    generateNutriReport = async () => {

        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        await this.calBMI()
        await this.calIBW()
        await this.calBMR()
        await this.calDER()
        await this.onOpenModal()




    }

    sendToNext = () => {
        this.props.history.push({
            pathname: "/userpreference",
            state: { der: Math.trunc(this.state.DER) }
        })
    }

    handletableSelect = async (value) => {
        // console.log(e,{value})
        // console.log(value)
        await this.setState({ PAL: value })
        // alert(this.state.PAL)
    }


    render() {

        const { selectedOption } = this.state;
        const { selectedOptionGender } = this.state;
        const { open } = this.state;

        console.log("BMI: ", this.state.bmi)
        console.log("BMR: ", this.state.bmr)
        console.log("DER: ", this.state.DER)




        return (

            <div>



                <Container>
                    <div>

                        <h1>Nutritional Screening </h1>
                        <br></br>
                    </div>
                    <Form>

                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="height">Height (cm)</Label>
                                    <Input placeholder="lg" bsSize="lg" type="text" name="height" id="height" placeholder="158 cm" onChange={this.onChange} value={this.state.height === null ? '' : this.state.height} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="weight">Weight (kg)</Label>
                                    <Input placeholder="lg" bsSize="lg" type="text" name="weight" id="weight" placeholder="46 Kg" onChange={this.onChange} value={this.state.weight === null ? '' : this.state.weight} />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="waist">Waist Circumference (cm)</Label>
                                    <Input placeholder="lg" bsSize="lg" type="text" name="waist" id="waist" placeholder="36 cm" onChange={this.onChange} value={this.state.waist === null ? '' : this.state.waist} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="hip">Hip Circumference (cm)</Label>
                                    <Input placeholder="lg" bsSize="lg" type="text" name="address" id="exampleAddress" placeholder="20 cm" onChange={this.onChange} value={this.state.hip === null ? '' : this.state.hip} />
                                </FormGroup>
                            </Col>

                        </Row>

                        <Row>





                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleSelect">Gender</Label>
                                    <Input type="select" name="gender" id="gender" onChange={this.onChange} value={this.state.gender === null ? '' : this.state.gender}>
                                        <option></option>
                                        <option>Male</option>
                                        <option>Female</option>

                                    </Input>
                                </FormGroup>
                            </Col>

                            <Col md={12}>
                                <Label for="exampleAddress">Select your Physical Activity Level</Label>
                                <Table hover>
                                    <thead>
                                        <tr>

                                            <th>Lifestyle</th>
                                            <th>Example</th>

                                            <th>PAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr onClick={() => this.handletableSelect("1.40")} style={{ backgroundColor: this.state.PAL === "1.40" ? 'lightgrey' : null }}>

                                            <td>Extremely inactive </td>
                                            <td>Cerebral palsy patient   <br /><img src="https://img.icons8.com/emoji/96/000000/person-in-bed.png" /></td>
                                            <td>less than 1.40</td>
                                        </tr>
                                        <tr onClick={() => this.handletableSelect("1.69")} style={{ backgroundColor: this.state.PAL === "1.69" ? 'lightgrey' : null }}>

                                            <td>Sedentary</td>
                                            <td>Office worker getting little or no exercise <br /> <img src="https://img.icons8.com/color/96/000000/hard-working.png" /></td>
                                            <td>1.40-1.69</td>
                                        </tr>
                                        <tr onClick={() => this.handletableSelect("1.99")} style={{ backgroundColor: this.state.PAL === "1.99" ? 'lightgrey' : null }}>

                                            <td>Moderately active	</td>
                                            <td>Construction worker or <br />person running one hour daily <br /> <img src="https://img.icons8.com/color/96/000000/worker-male--v1.png" /> or <img src="https://img.icons8.com/color/96/000000/running--v1.png" /></td>
                                            <td>1.70-1.99</td>
                                        </tr>
                                        <tr onClick={() => this.handletableSelect("2.40")} style={{ backgroundColor: this.state.PAL === "2.40" ? 'lightgrey' : null }}>

                                            <td>Vigorously active</td>
                                            <td>Agricultural worker  (non mechanized) <br />or person swimming two hours daily <br /> <img src="https://img.icons8.com/color/96/000000/farmer-male--v1.png" /> or <img src="https://img.icons8.com/color/96/000000/swimming.png" /></td>
                                            <td>2.00-2.40</td>
                                        </tr>
                                        <tr onClick={() => this.handletableSelect("3.40")} style={{ backgroundColor: this.state.PAL === "3.40" ? 'lightgrey' : null }}>

                                            <td>Extremely active</td>
                                            <td>Competitive cyclist <br /> <img src="https://img.icons8.com/color/96/000000/cycling-track--v1.png" /></td>
                                            <td> 2.40</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>

                        </Row>
                    </Form>

                    <Container>
                        <Button color="primary"  size="lg" onClick={this.generateNutriReport}>View Nutritional Report </Button>

                        <Modal open={open} onClose={this.onCloseModal} center>
                            <Label> <h2>Nutritional Report  <img src="https://img.icons8.com/color/48/000000/caloric-energy.png" /></h2></Label>
                            <Table hover>
                                <thead>
                                    <tbody>
                                        <tr>

                                            <th class="align-middle">Body Mass Index </th>

                                            <td class="align-middle">{Math.trunc(this.state.bmi)}</td>
                                            <td class="align-middle">{this.getBmi(this.state.bmi)}</td>


                                        </tr>


                                        <tr>
                                            <th class="align-middle">Ideal Body Weight </th>
                                            <td class="align-middle">{Math.trunc(this.state.ibw)}  kg </td>
                                            <td class="align-middle"> {this.getIBW()}</td>

                                        </tr>
                                        <tr>

                                            <th class="align-middle">Basal Metabolic Rate</th>
                                            <td class="align-middle">{Math.trunc(this.state.bmr)}  </td>


                                        </tr>
                                        <tr>

                                            <th class="align-middle">Daily Energy Requirment </th>
                                            <td class="align-middle">{Math.trunc(this.state.DER)} Calories</td>

                                        </tr>
                                        <tr>

                                            <th class="align-middle">{this.getWaist(this.state.waist, this.state.gender)} </th>


                                        </tr>
                                    </tbody>
                                </thead>




                            </Table>

                            <div>

                                <Button color="success" onClick={() => this.saveNutritionalScreening()}>Save Data</Button>
                            </div>
                        </Modal>

                    </Container>

                    <Container>
                        <Button size="lg" color="info" onClick={this.sendToNext} block >Next</Button>
                    </Container>








                </Container>






            </div>

        )
    }
}

export default withRouter(NutritionalAssessment)