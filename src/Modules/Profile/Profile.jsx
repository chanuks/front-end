import React, { Component } from 'react'
import { Jumbotron, Button, Container, Row, Col, Spinner, Table, Card, CardHeader, CardBody, Label } from 'reactstrap';
import moment from 'moment';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';

export class Profile extends Component {

    constructor(props){
        super(props);
        this.state={
            
            userInfo:null,
            nutritionalScreenings:[],
            openModal:false,
            modalData:null
        }
    }

    userInformation;

    // state = {
      
    //     userInfo:null,
    //     nutritionalScreenings:[]
    // }

    getBmi = (bmi) => {
        if (bmi < 18.5) {
            return <div class="p-3 mb-2 bg-danger text-white">Under Weight</div>;
        }
        if (bmi >= 18.5 && bmi < 22.9) {
            return <div class="p-3 mb-2 bg-success text-white">Normal Weight</div>;
        }
        if (bmi >= 23 && bmi < 24.9) {
            return <div class="p-3 mb-2 bg-warning text-dark">Over Weight</div>;
        }
        if (bmi >= 25 && bmi < 30) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 1</div>;
        }
        if (bmi >= 30 && bmi < 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 2</div>;
        }
        if (bmi >= 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 3</div>;
        }
    }

    getIBW = () => {
        return "The healthy body weight based on your Age,Gender & Height"
    }


    componentDidMount(){
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));
        console.log("User", this.userInformation.username)
        this.getUserInfo()
        this.getNutritionalScreenings()
    }


    getUserInfo=()=>{
        fetch(`http://localhost:8080/getUserInfo/${this.userInformation.userId}`, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(item => {
              
                this.setState({
                  userInfo: item
                })
                console.log(this.state.userInfo)
            })
            .catch(err => console.log(err))
    }



    getNutritionalScreenings=()=>{
        fetch(`http://localhost:8080/nutripulz/users/${this.userInformation.userId}/nutritionalScreenings`, {
            method: 'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(item => {
                let data = item._embedded.nutritionalScreenings
              
                this.setState({
                    nutritionalScreenings: data
                })
                console.log(this.state.nutritionalScreenings)
            })
            .catch(err => console.log(err))
    }
      

    viewModal=async (data)=>{
        // alert("modal")
        await this.setState({
            modalData:data,
            openModal:true
        })

        console.log(this.state.modalData)
        // alert(this.state.open)
        // await this.setState({
        //     open:true
        // })
    }
      
    onCloseModal=()=>{
        this.setState({
            openModal:false
        })
    }

    render() {
        return (
            <div>
                <Jumbotron>
                <Row>
              <Col>
                <>
                <h1 className="display-3"> <img src="https://img.icons8.com/color/96/000000/person-male.png"/>Profile</h1>
                <h4 className="lead">Your nutritional health status.. </h4>
                <hr className="my-2" />
                </>
              </Col>
              <Col>
               
                {this.state.userInfo===null?
                <Spinner color="primary"/>
                :
                <>
                <Row>
                  <Col >
                    <Table hover>
                    <tbody>
                      <tr>
                        <th scope="row">Energy Requirement </th>
                <td>{this.state.userInfo.der}</td>
                      </tr>
                      <tr>
                        <th scope="row">Eating Pattern</th>
                <td>{this.state.userInfo.eatingPattern}</td>
                      </tr>
                      <tr>
                        <th scope="row">User Allergies</th>
                        {this.state.userInfo.userAllergies.map((item,index)=>{
                            return(
                                <td key={index}>{item}</td>
                            )
                        })}
                      </tr>
                      <tr>
                        <th scope="row">Medical Conditions</th>
                        {this.state.userInfo.userMedicalConditions.map((item,index)=>{
                            return(
                                <td key={index}>{item}</td>
                            )
                        })}
                      </tr>
                    </tbody>
                  </Table>
                  </Col>
                  {/* <Col>
                  <Table hover>
                    <tbody>
                      <tr>
                        <th scope="row">BMR</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th scope="row">BMI</th>
                        <td></td>
                      </tr>
                      <tr>
                        <th scope="row">IBW</th>
                        <td></td>
                      </tr>
                    </tbody>
                  </Table>
                  </Col> */}
                </Row>
                </>
                }
              </Col>
            </Row>
                </Jumbotron>
                <Container>
                {this.state.nutritionalScreenings===null?
                <Spinner color="primary"/>
                :
                <>
                    <Row style={{ paddingLeft: '3vh' }}>
                    {this.state.nutritionalScreenings.map((item,index)=>{
                        return(
                            <Card onClick={()=>{this.viewModal(item)}} style={{ margin: '2vh', marginLeft: '3vh' }} key={index}>
                                <CardHeader>Nutritional Screening {index+1}</CardHeader>
                        <CardBody>{moment(item.date).format('LLL')}
                        
                        </CardBody>
                            </Card>
                        )
                    })}
                    </Row>
                </> 
                }
                </Container>

                <Modal open={this.state.openModal} onClose={this.onCloseModal} center>
                    <Label> <h2>Nutritional Report  <img src="https://img.icons8.com/color/48/000000/caloric-energy.png" /></h2></Label>
                    <Table hover>
                        <thead>
                            <tbody>
                                <tr>

                                    <th class="align-middle">Body Mass Index </th>

                                    <td class="align-middle">{Math.trunc(this.state.modalData!==null && this.state.modalData.bmi)}</td>
                                    <td class="align-middle">{this.getBmi(this.state.modalData!==null && this.state.modalData.bmi)}</td>


                                </tr>


                                <tr>
                                    <th class="align-middle">Ideal Body Weight </th>
                                    <td class="align-middle">{Math.trunc(this.state.modalData!==null && this.state.modalData.ibw)}  kg </td>
                                    <td class="align-middle"> {this.getIBW(this.state.modalData!==null && this.state.modalData.ibw)}</td>

                                </tr>
                                <tr>

                                    <th class="align-middle">Basal Metabolic Rate</th>
                                    <td class="align-middle">{Math.trunc(this.state.modalData!==null && this.state.modalData.bmr)}  </td>


                                </tr>
                                <tr>

                                    <th class="align-middle">Daily Energy Requirment </th>
                                    <td class="align-middle">{Math.trunc(this.state.modalData!==null && this.state.modalData.der)} Calories</td>

                                </tr>
                            </tbody>
                        </thead>




                    </Table>

                    {/* <div>

                        <Button color="success" onClick={() => this.saveNutritionalScreening()}>Save Data</Button>
                    </div> */}
                </Modal>
            </div>
        )
    }
}

export default Profile
