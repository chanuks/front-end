import React from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { render } from 'react-dom';
import TopNav from '../../Components/TopNav'
import { withRouter } from 'react-router-dom';
class SignUp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      firstname: '',
      lastname: '',
      birthdate: '',
      region: '',
      category: '',
      gender: ''

    };
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  componentDidMount() {
    // if item exists, populate the state with proper data
    if (this.props.item) {
      const { username,
        password,
        firstname,
        lastname,
        birthdate,
        region,
        category,
        gender } = this.props.item
      this.setState({
        username,
        password,
        firstname,
        lastname,
        birthdate,
        region,
        category,
        gender
      })
    }
  }

  submitFormAdd = e => {

    fetch('http://localhost:8080/restUsers/saveUser', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({

        username: this.state.username,
        password: this.state.password,
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        birthdate: this.state.birthdate,
        region: this.state.region,
        category: this.state.category,
        gender: this.state.gender

      })
    })
      .then(response => response.json())
      .then(item => {

        
          localStorage.setItem("userInfo", JSON.stringify(item));
          console.log('test cred',item);
          this.props.close()
           console.log("User Id REGG",item.userId)
           this.props.history.push("/nutriassess")
          
      
        // this.props.close()
        // console.log("User Id",item.userId)
        // this.props.history.push("/nutriassess")
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <div>

        <Container> <Form onSubmit={this.submitFormAdd}>

          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="firstName">Firstname</Label>
                <Input type="text" name="firstname" id="firstname" onChange={this.onChange} value={this.state.firstname === null ? '' : this.state.firstname} />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="lastName">Lastname</Label>
                <Input type="text" name="lastname" id="lastname" onChange={this.onChange} value={this.state.lastname === null ? '' : this.state.lastname} />
              </FormGroup>
            </Col>
          </Row>

          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="username">Username</Label>
                <Input type="text" name="username" id="username" onChange={this.onChange} value={this.state.username === null ? '' : this.state.username} />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="password">Password</Label>
                <Input type="password" name="password" id="password" onChange={this.onChange} value={this.state.password === null ? '' : this.state.password} />
              </FormGroup>
            </Col>
          </Row>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="address">Region</Label>
                <Input type="select" name="region" id="region" onChange={this.onChange} value={this.state.region === null ? '' : this.state.region}>
                  <option>Ampara </option>
                  <option>Anuradhapura </option>
                  <option>Badulla</option>
                  <option>Batticaloa	</option>
                  <option>Colombo</option>
                  <option>Galle</option>
                  <option>Gampaha</option>
                  <option>Hambantota</option>
                  <option>Jaffna</option>
                  <option>Kalutara</option>
                  <option>Kandy</option>
                  <option>Kegalle</option>
                  <option>Kurunegala</option>
                  <option>Mannar</option>
                  <option>Matale</option>
                  <option>Matara</option>
                  <option>Monaragala</option>
                  <option>Mullaitivu</option>
                  <option>Nuwara Eliya</option>
                  <option>Polonnaruwa</option>
                  <option>Puttalam</option>
                  <option>Ratnapura</option>
                  <option>Trincomalee</option>
                  <option>Vavuniya</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>


          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="birthDate">Date Of Birth</Label>
                <Input type="date" name="birthdate" id="birthdate" onChange={this.onChange} value={this.state.birthdate === null ? '' : this.state.birthdate} />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="gender">Gender</Label>
                <Input type="select" name="gender" id="gender" onChange={this.onChange} value={this.state.gender === null ? '' : this.state.gender}>

                  <option> </option>
                  <option>Male</option>
                  <option>Female</option>


                </Input>
              </FormGroup>
            </Col>
          </Row>

          <Row form>
            <Col md={12}>
              <FormGroup>
                <Label for="category">Household Category</Label>
                <Input type="select" name="category" id="category" onChange={this.onChange} value={this.state.category === null ? '' : this.state.category} >
                  <option >Staying at home </option>
                  <option>Engaging in a job</option>
                  <option>Engaging in studies</option>
                  </Input>
              </FormGroup>
            </Col>
          </Row>





          <Button onClick={() => this.submitFormAdd()}>Sign in</Button>
        </Form></Container>

      </div>

    );
  }

}

export default withRouter (SignUp);