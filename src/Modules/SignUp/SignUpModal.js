
import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import SignUp from './SignUp'
class SignUpModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false
    }
  }

  // static getDerivedStateFromProps(nextProps,prevState){
  //   if(nextProps.open!==null && prevState.modal !== nextProps.open){
  //     return{
  //       modal:nextProps.open
  //     }
  //   }
  //   else return null
  // }

  toggle = () => {
    if(this.props.close){
      this.props.close()
    }else{
      this.setState(prevState => ({
        modal: !prevState.modal
      }))
    }
  }

  render() {
      const closeBtn = <button className="close" onClick={this.toggle}>&times;</button>

      const label = this.props.buttonLabel

      let button = ''
      let title = ''

      if(label === 'Edit'){
        button = <Button
                  color="warning"
                  onClick={this.toggle}
                  style={{float: "left", marginRight:"10px"}}>{label}
                </Button>
        title = 'Edit Item'
      } else {
        button = <Button
                  color="success"
                  outline color="primary"
                  onClick={this.toggle}
                  style={{float: "left", marginRight:"10px"}}>{label}
                </Button>
        title = 'Sign Up for a Healthy Life'
      }


      return (
      <div>
        {button}
        <Modal isOpen={this.state.modal || this.props.open} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle} close={closeBtn}>Sign Up for a Healthy Life</ModalHeader>
          <ModalBody>
            <SignUp
              close={this.toggle}
              addItemToState={this.props.addItemToState}
              updateState={this.props.updateState}
              toggle={this.toggle}
              item={this.props.item} />
          </ModalBody>
        </Modal>
      </div>
    )
  }
}

export default SignUpModal 