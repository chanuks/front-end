import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'

import { Jumbotron, Button, Container, Row, Col, Spinner, Table } from 'reactstrap';




class UserHome extends Component {
    userInformation;
    state = {
      
        username:'',
        latest:null
    }

    componentDidMount(){
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));
        console.log("User", this.userInformation.username)
        this.setState({ username: this.userInformation.username})
        this.getLatest();
    }

    getLatest=()=>{
      fetch(`http://localhost:8080/nutripulz/users/${this.userInformation.userId}/nutritionalScreenings`, {
          method: 'get',
          headers: {
              'Content-Type': 'application/json'
          }
      })
          .then(response => response.json())
          .then(item => {
              let data = item._embedded.nutritionalScreenings
              console.log(data)
              let selected = data[data.length-1]
              this.setState({
                latest: selected
              })
              console.log(this.state.latest)
          })
          .catch(err => console.log(err))
  }
    

  render() {

    
    return (

      <div>


        <Jumbotron>
          {/* <Container> */}
            <Row>
              <Col>
                <>
                <h1 className="display-3">Retake your nutritional assessment here...</h1>
                <h4 className="lead">Find out about your nutritional state by taking our nutritional assesmment..</h4>
                <hr className="my-2" />
                
                <p className="lead">
                  <Button href='/nutriassess' color="success">Nutritional Assessment <img src="https://img.icons8.com/color/48/000000/health-checkup.png"/></Button>
                </p>          
                </>
              </Col>
              <Col>
                <h4>Your latest health screening</h4>
                <hr className="my-2" />
                {this.state.latest===null?
                <Spinner color="primary"/>
                :
                <>
                <Row>
                  <Col >
                    <Table hover>
                    <tbody>
                      <tr>
                        <th scope="row">Height</th>
                        <td>{this.state.latest.height}</td>
                      </tr>
                      <tr>
                        <th scope="row">Weight</th>
                        <td>{this.state.latest.weight}</td>
                      </tr>
                      <tr>
                        <th scope="row">Waist</th>
                        <td>{this.state.latest.waist}</td>
                      </tr>
                      <tr>
                        <th scope="row">Hip</th>
                        <td>{this.state.latest.hip}</td>
                      </tr>
                    </tbody>
                  </Table>
                  </Col>
                  <Col>
                  <Table hover>
                    <tbody>
                      <tr>
                        <th scope="row">BMR</th>
                        <td>{this.state.latest.bmr}</td>
                      </tr>
                      <tr>
                        <th scope="row">BMI</th>
                        <td>{this.state.latest.bmi}</td>
                      </tr>
                      <tr>
                        <th scope="row">IBW</th>
                        <td>{this.state.latest.ibw}</td>
                      </tr>
                    </tbody>
                  </Table>
                  </Col>
                </Row>
                </>
                }
              </Col>
            </Row>
          {/* </Container> */}
        </Jumbotron>
        <Carousel />



      </div>

    )
  }

}
export default UserHome