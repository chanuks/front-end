import React, { Component, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css'
import Select from 'react-select';

import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import moment from "moment";
import $ from 'jquery';
import MultiSelect from "react-multi-select-component";

import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import InputM from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import SelectM from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

import { Jumbotron, Col, Row, Button, Form, FormGroup, Label, Input, Container, Table } from 'reactstrap';


const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }));
  
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
  ];
  
  function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }
  

const options = [
    { label: "Grapes 🍇", value: "grapes" },
    { label: "Mango 🥭", value: "mango" },
    { label: "Strawberry 🍓", value: "strawberry" },
];

const optionsMedical = [
    { value: 'Diabetes', label: 'Diabetes' },
    { value: 'Hypertension', label: 'Hypertension' },
    { value: 'Hyperlipidemia', label: 'Hyperlipidemia' },
    { value: 'Obesity', label: 'Obesity' },
    { value: 'Chronic Kidney Disease', label: 'Chronic Kidney Disease' },
    { value: 'Chronic Liver Disease', label: 'Chronic Liver Disease' },
    { value: 'No Conditions', label: 'I am all good!' },
];

const optionsAllergies = [
    { value: 'Milk', label: <div><img src="https://img.icons8.com/color/48/000000/no-milk.png" />Cow's Milk</div> },
    { value: 'Egg', label: <div><img src="https://img.icons8.com/color/48/000000/no-eggs.png" />Eggs</div> },
    { value: 'TreeNuts', label: <div><img src="https://img.icons8.com/color/48/000000/no-nuts.png" />Tree Nuts</div> },
    { value: 'Peanuts', label: <div><img src="https://img.icons8.com/color/48/000000/no-peanut.png" /> Peanuts</div> },
    { value: 'Shellfish', label: <div><img src="https://img.icons8.com/color/48/000000/no-shellfish.png" />Shellfish</div> },
    { value: 'Wheat', label: <div><img src="https://img.icons8.com/color/48/000000/no-gluten.png" />Wheat</div> },
    { value: 'Soy', label: <div><img src="https://img.icons8.com/color/48/000000/no-soy.png" />Soy</div> },
    { value: 'Fish', label: <div><img src="https://img.icons8.com/color/48/000000/no-fish.png" />Fish</div> },
];

const optionsGender = [
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },

];

//




class UserPreference extends Component {


    userInformation;
    constructor(props) {
        super(props);

        this.state = { value: 10 };
    }

    
      


    state = {
        selectedOption: null,
        selectedOptionGender: null,
        height: 0,
        weight: 0,
        waist: null,
        hip: null,
        bmi: null,
        bmr: null,
        gender: '',
        ibw: null,
        DER: null,
        PAL: null,
        riskFactors: '',
        date: new Date(),
        open: false,
        selected: null,
        setSelected: [],
        breakfastFood: '',
        lunchFood:'',
        dinnerFood:'',
        userAllergies:[],
        userMedicalConditions:[],
        DER:null,


    };


    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    handleChange = selectedOption => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption.value);
        this.setState({ PAL: selectedOption.value })

        console.log(`PAL:` + this.state.PAL);
    };
    handleChangeMedical = selectedOptionMedical => {
        this.setState({ selectedOptionMedical });



    };
    handleChangeGender = selectedOptionGender => {
        this.setState({ selectedOptionGender });
        console.log(`Option selected:`, selectedOptionGender.value);
    };
    onChange = e => {

        this.setState({ [e.target.name]: e.target.value })

    }

    calBMI = () => {
        this.setState({ bmi: (this.state.weight / Math.pow((this.state.height / 100), 2)) })
        console.log("bmi:" + this.state.bmi);

    }

    calIBW = () => {

        this.setState({ ibw: (23 * (Math.pow((this.state.height / 100), 2))) })

        console.log("ibw: " + this.state.ibw);
        console.log("gender: " + this.state.gender);
    }

    calBMR = () => {



        if (this.state.gender == "Male") {
            this.setState({ bmr: (1 * Math.trunc(this.state.ibw) * 24) })


        }
        if (this.state.gender == "Female") {
            this.setState({ bmr: (0.9 * Math.trunc(this.state.ibw) * 24) })


        }


    }

    calDER = () => {
        this.setState({ DER: this.state.bmr * this.state.PAL })

        console.log("der" + this.state.DER);
    }


    getBmi = (bmi) => {
        if (bmi < 18.5) {
            return <div class="p-3 mb-2 bg-danger text-white">Under Weight</div>;
        }
        if (bmi >= 18.5 && bmi < 22.9) {
            return <div class="p-3 mb-2 bg-success text-white">Normal Weight</div>;
        }
        if (bmi >= 23 && bmi < 24.9) {
            return <div class="p-3 mb-2 bg-warning text-dark">Over Weight</div>;
        }
        if (bmi >= 25 && bmi < 30) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 1</div>;
        }
        if (bmi >= 30 && bmi < 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 2</div>;
        }
        if (bmi >= 35) {
            return <div class="p-3 mb-2 bg-danger text-white">Obesity - class 3</div>;
        }
    }

    getIBW = () => {
        return "The healthy body weight based on your Age,Gender & Height"
    }



    
    submitFormAdd = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        fetch('http://localhost:8080/nutripulz/userInfoes', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                der: "2000",
                breakfastFood: "cereals",
                lunchFood: "rice",
                dinnerFood: "bread",
                eatingPattern:"anything",
                userAllergies: ["treenuts","pineapple"],
                userMedicalConditions: ["diabetes","hypertension"],
                user: "http://localhost:8080/nutripulz/users/" +this.userInformation.userId,



            })
        })
            .then(response => response.json())
            .then(item => {
                if (Array.isArray(item)) {
                    this.props.addItemToState(item[0])
                    this.props.toggle()

                } else {
                    console.log('failure')
                }
            })
            .catch(err => console.log(err))
    }

    generateNutriReport = async () => {

        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        await this.calBMI()
        await this.calIBW()
        await this.calBMR()
        await this.calDER()
        await this.onOpenModal()




    }




    render() {

        const { selectedOption } = this.state;
        const { selectedOptionGender } = this.state;
        const { open } = this.state;


        console.log("BMI: ", this.state.bmi)
        console.log("BMR: ", this.state.bmr)
        console.log("DER: ", this.state.DER)






        return (




            <div>

                <div>
                    <Jumbotron fluid>
                        <Container fluid>
                            <h3 className="display-3">Tell us more about what you like to eat..</h3>

                        </Container>
                    </Jumbotron>
                </div>


                <Container>
                    <Form>

                        <Row>
                            <Col md={6}>
                                <label><h4>Please select the food item you like the Most for each meal.</h4></label>
                                <FormGroup>
                                
                                    <Label size="lg" for="exampleSelect"><img src="https://img.icons8.com/color/96/000000/breakfast.png" /> Breakfast </Label>
                                    <Input bsSize="lg" type="select" name="breakfastFood" id="breakfastFood" onChange={this.onChange} value={this.state.breakfastFood === null ? '' : this.state.breakfastFood} >
                                        <option data-image="icons/icon_calendar.gif">Rice</option>
                                        <option>Bread</option>
                                        <option>Rice flour based foods</option>
                                        <option>Wheat flour based foods </option>
                                        <option>Yams</option>
                                        <option>Grams</option>
                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                        <Row>
                            <Col md={6}>

                                <FormGroup>
                                    <Label size="lg" for="exampleSelect" > <img src="https://img.icons8.com/color/96/000000/lunch.png" /> Lunch</Label>
                                    <Input bsSize="lg" type="select" name="lunchFood" id="lunchFood" onChange={this.onChange} value={this.state.lunchFood === null ? '' : this.state.lunchFood} >
                                        <option>Rice</option>
                                        <option>Bread</option>
                                        <option>Wheat flour based foods </option>
                                        <option>Grams</option>
                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                        <Row>
                            <Col md={6}>

                                <FormGroup>
                                    <Label size="lg" for="exampleSelect"><img src="https://img.icons8.com/color/96/000000/dinner.png" /> Dinner </Label>
                                    <Input bsSize="lg" type="select" name="dinnerFood" id="dinnerFood" onChange={this.onChange} value={this.state.dinnerFood === null ? '' : this.state.dinnerFood} >
                                        <option>Rice</option>
                                        <option>Bread</option>
                                        <option>Rice flour based foods</option>
                                        <option>Wheat flour based foods </option>

                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                    </Form>
                </Container>
                <Container>
                    <Form>
                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/sneeze.png" />Test </Label>
                                    <div>
                                        <h1>Select Fruits</h1>
                                        <pre>{JSON.stringify(this.state.selected)}</pre>
                                        <MultiSelect
                                            options={optionsAllergies}
                                            value={this.state.selected}
                                            onChange={this.state.setSelected}
                                            labelledBy={"Select"}
                                        />
                                    </div>

                                </FormGroup>
                            </Col>

                        </Row>
                        


                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/sneeze.png" />Food Allergies & Intolerances </Label>
                                    {/* <div>
                                    <pre>{JSON.stringify(selected)}</pre>
                                    <MultiSelect
                                        options={optionsAllergies}
                                        value={selected}
                                        onChange={setSelected}
                                        labelledBy={"Select"}
                                    />
                                    </div> */}

                                    <Select bsSize="lg" multiple
                                        value={this.state.selectedOption}
                                        onChange={this.handleChange}
                                        options={optionsAllergies}
                                    />
                                    {/* <Input type="select" name="selectMulti" id="exampleSelectMulti" multiple onChange={this.handleChange} value={this.state.selectValue}>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input> */}
                                    {/* <Input type="text" name="address" id="exampleAddress" placeholder="" /> */}
                                </FormGroup>
                            </Col>

                        </Row>







                        <Row>
                            <Col md={6}>
                                <FormGroup>
                                    <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/health-book.png" />Please specify if you have any medical conditions</Label>
                                    <Select bsSize="lg"
                                        value={this.state.selectedOptionMedical}
                                        onChange={this.handleChangeMedical}
                                        options={optionsMedical}
                                    />
                                    {/* <Input type="text" name="address" id="exampleAddress" placeholder="" /> */}
                                </FormGroup>
                            </Col>

                        </Row>
                        <Row>
                            <Col md={12}>
                                <FormGroup>
                                    <Label size="lg" for="exampleSelect"> Please specify your eating pattern (Please refer the guide below) </Label>
                                    <Input bsSize="lg" type="select" name="eatingPattern" id="eatingPattern" onChange={this.onChange} value={this.state.eatingPattern === null ? '' : this.state.eatingPattern} >
                                        <option>Lacto-ovo-vegetarian</option>
                                        <option>Lacto-vegetarian</option>
                                        <option>Ovo-vegetarian</option>
                                        <option>Pesco-vegetarian</option>
                                        <option>Semi-vegetarian</option>
                                        <option>Vegan</option>
                                        <option>I eat Anything!</option>
                                    </Input>
                                </FormGroup>
                            </Col>

                        </Row>


                        <Col md={12}>
                            <Label for="exampleAddress">Eating Patterns Guide</Label>
                            <Table hover>
                                <thead>
                                    <tr>

                                        <th>Type</th>
                                        <th>Description</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td><h6><b>Lacto-ovo-vegetarians </b></h6></td>
                                        <td> If you eat plant-based foods, dairy products, and eggs, and exclude meat, poultry, and fish. <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /></td>

                                    </tr>
                                    <tr>

                                        <td><h6><b>Lacto-vegetarians</b></h6></td>
                                        <td>If you eat plant-based foods and dairy products, and exclude meat, poultry, fish, and eggs. <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /><img src="https://img.icons8.com/color/48/000000/no-eggs.png" /></td>
                                        <td></td>

                                    </tr>
                                    <tr>

                                        <td><h6><b>Ovo-vegetarians </b></h6></td>
                                        <td>If you eat plant-based foods and eggs, and exclude meat, poultry, fish, and dairy products. <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /><img src="https://img.icons8.com/color/48/000000/no-milk.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td><h6><b>Pesco-vegetarians </b></h6></td>
                                        <td>If you eat a vegetarian diet but also include fish. <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/fish-food.png" /><img src="https://img.icons8.com/color/48/000000/no-milk.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-eggs.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td><h6><b>Semi-vegetarians </b></h6></td>
                                        <td>You may eat dairy products or eggs, as well as a little fish and chicken, and generally exclude meat; <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/fish-food.png" /><img src="https://img.icons8.com/color/48/000000/thanksgiving.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>

                                        <td><h6><b>Vegans</b></h6></td>
                                        <td>If you eat plant-based foods only, excluding all foods of animal origin <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /></td>

                                    </tr>
                                </tbody>
                            </Table>
                        </Col>


                    </Form>

                    <Container>
                        <Button onClick={this.generateNutriReport}>View Nutritional Report </Button>
                        <Modal open={open} onClose={this.onCloseModal} center>
                            <Label> <h2>Nutritional Report  <img src="https://img.icons8.com/color/48/000000/caloric-energy.png" /></h2></Label>
                            <Table hover>
                                <thead>
                                    <tbody>
                                        <tr>

                                            <th class="align-middle">Body Mass Index </th>

                                            <td class="align-middle">{Math.trunc(this.state.bmi)}</td>
                                            <td class="align-middle">{this.getBmi(this.state.bmi)}</td>


                                        </tr>


                                        <tr>
                                            <th class="align-middle">Ideal Body Weight </th>
                                            <td class="align-middle">{Math.trunc(this.state.ibw)}  kg </td>
                                            <td class="align-middle"> {this.getIBW()}</td>

                                        </tr>
                                        <tr>

                                            <th class="align-middle">Basal Metabolic Rate</th>
                                            <td class="align-middle">{Math.trunc(this.state.bmr)}  </td>


                                        </tr>
                                        <tr>

                                            <th class="align-middle">Daily Energy Requirment </th>
                                            <td class="align-middle">{Math.trunc(this.state.DER)} Calories</td>

                                        </tr>
                                    </tbody>
                                </thead>




                            </Table>

                            <div>

                                <Button color="success" onClick={() => this.submitFormAdd()}>Save Data</Button>
                            </div>
                        </Modal>

                    </Container>








                </Container>






            </div>

        )
    }
}

export default UserPreference