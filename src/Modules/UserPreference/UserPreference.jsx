import React, { Component, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import TopNav from '../../Components/TopNav'
import Carousel from '../../Components/Carousel'
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css'
import Select from 'react-select';

import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import moment from "moment";
import $ from 'jquery';
import MultiSelect from "react-multi-select-component";



import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import InputM from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItemM from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import SelectM from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import multi from './multi';

import { Jumbotron, Col, Row, Button, Form, FormGroup, Label, Input, Container, Table } from 'reactstrap';






const options = [
    { label: "Grapes 🍇", value: "grapes" },
    { label: "Mango 🥭", value: "mango" },
    { label: "Strawberry 🍓", value: "strawberry" },
];

const optionsMedical = [
    { value: 'Diabetes', label: 'Diabetes' },
    { value: 'Hypertension', label: 'Hypertension' },
    { value: 'Hyperlipidemia', label: 'Hyperlipidemia' },
    { value: 'Obesity', label: 'Obesity' },
    { value: 'Chronic Kidney Disease', label: 'Chronic Kidney Disease' },
    { value: 'Chronic Liver Disease', label: 'Chronic Liver Disease' },
    { value: 'No Conditions', label: 'I am all good!' },
];

const optionsAllergies = [
    { value: 'Milk', label: <div><img src="https://img.icons8.com/color/48/000000/no-milk.png" />Cow's Milk</div> },
    { value: 'Egg', label: <div><img src="https://img.icons8.com/color/48/000000/no-eggs.png" />Eggs</div> },
    { value: 'TreeNuts', label: <div><img src="https://img.icons8.com/color/48/000000/no-nuts.png" />Tree Nuts</div> },
    { value: 'Peanuts', label: <div><img src="https://img.icons8.com/color/48/000000/no-peanut.png" /> Peanuts</div> },
    { value: 'Shellfish', label: <div><img src="https://img.icons8.com/color/48/000000/no-shellfish.png" />Shellfish</div> },
    { value: 'Wheat', label: <div><img src="https://img.icons8.com/color/48/000000/no-gluten.png" />Wheat</div> },
    { value: 'Soy', label: <div><img src="https://img.icons8.com/color/48/000000/no-soy.png" />Soy</div> },
    { value: 'Fish', label: <div><img src="https://img.icons8.com/color/48/000000/no-fish.png" />Fish</div> },
];

const optionsGender = [
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' },

];

//




class UserPreference extends Component {



    userInformation;
    constructor(props) {
        super(props);

        this.state = { value: 10, selected: [], selectedMed: [], finalizedAllergies: [], finalizedMeds: [] };
    }








    state = {
        selectedOption: null,
        selectedOptionGender: null,
        height: 0,
        weight: 0,
        waist: null,
        hip: null,
        bmi: null,
        bmr: null,
        gender: '',
        ibw: null,
        DER: null,
        PAL: null,
        riskFactors: '',
        date: new Date(),
        open: false,
        // selected: [],
        setSelected: [],
        breakfastFood: '',
        lunchFood: '',
        dinnerFood: '',
        userAllergies: [],
        userMedicalConditions: [],
        DER: null,
        array: [],
        personName: '',
        name: '',


    };

    componentDidMount() {
        console.log(this.props.location.state.der);
        if (this.props.location.state) {
            this.setState({
                der: this.props.location.state.der
            })
        } else {
            alert("Go back der eka one")
        }
    }

    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    // handleChangeMultiple = (event) => {
    //     let data = event.target.value;
    //     let arrayCopy = this.state.array
    //     arrayCopy.push(data)
    //     this.setState({
    //         array: arrayCopy
    //     })
    // };

    handleChange = selectedOption => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption.value);
        this.setState({ PAL: selectedOption.value })

        console.log(`PAL:` + this.state.PAL);
    };
    handleChangeMedical = selectedOptionMedical => {
        this.setState({ selectedOptionMedical });



    };
    handleChangeGender = selectedOptionGender => {
        this.setState({ selectedOptionGender });
        console.log(`Option selected:`, selectedOptionGender.value);
    };
    onChange = e => {

        this.setState({ [e.target.name]: e.target.value })

    }




    submitFormAdd = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        fetch('http://localhost:8080/nutripulz/userInfoes', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                der: "2000",
                breakfastFood: "cereals",
                lunchFood: "rice",
                dinnerFood: "bread",
                eatingPattern: "anything",
                userAllergies: ["treenuts", "pineapple"],
                userMedicalConditions: ["diabetes", "hypertension"],
                user: "http://localhost:8080/nutripulz/users/" + this.userInformation.userId,



            })
        })
            .then(response => response.json())
            .then(item => {
                if (Array.isArray(item)) {
                    this.props.addItemToState(item[0])
                    this.props.toggle()

                } else {
                    console.log('failure')
                }
            })
            .catch(err => console.log(err))
    }




    saveUserInfo = e => {
        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        console.log("User gender", this.userInformation.gender)

        let obj = {
            der: this.state.der,
            breakfastFood: this.state.breakfastFood,
            lunchFood: this.state.lunchFood,
            dinnerFood: this.state.dinnerFood,
            eatingPattern: this.state.eatingPattern,
            userAllergies: this.state.finalizedAllergies,
            userMedicalConditions: this.state.finalizedMeds,
            userId: this.userInformation.userId,
        }

        fetch('http://localhost:8080/nutripulz/userInfoes', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                der: this.state.der,
                breakfastFood: this.state.breakfastFood,
                lunchFood: this.state.lunchFood,
                dinnerFood: this.state.dinnerFood,
                eatingPattern: this.state.eatingPattern,
                userAllergies: this.state.finalizedAllergies,
                userMedicalConditions: this.state.finalizedMeds,
                user: "http://localhost:8080/nutripulz/users/" + this.userInformation.userId,



            })
        })
            .then(response => response.json())
            .then(item => {
                if (Array.isArray(item)) {
                    this.props.addItemToState(item[0])
                    this.props.toggle()

                } else {
                    console.log('failure')
                }

                this.props.history.push({
                    pathname: "/mealPlan",
                    state: { data: obj }
                })
            })
            .catch(err => console.log(err))
    }




    generateNutriReport = async () => {

        this.userInformation = JSON.parse(localStorage.getItem('userInfo'));


        console.log("User", this.userInformation.userId)
        await this.calBMI()
        await this.calIBW()
        await this.calBMR()
        await this.calDER()
        await this.onOpenModal()




    }

    handleChangeMultiple = (option) => {
        console.log("option", option)
        let array = []
        let finalArray = []
        option.map((item) => {
            let obj = {
                label: item.value,
                value: item.value
            }
            finalArray.push(item.value)
            array.push(obj)
        })

        this.setState({
            selected: array,
            finalizedAllergies: finalArray
        })
    }

    handleChangeMultipleMed = (option) => {
        console.log("option", option)
        let array = []
        let finalArray = []
        option.map((item) => {
            let obj = {
                label: item.value,
                value: item.value
            }
            finalArray.push(item.value)
            array.push(obj)
        })

        this.setState({
            selectedMed: array,
            finalizedMeds: finalArray
        })
    }

    handleTableSelect = async (value) => {
        await this.setState({
            eatingPattern: value
        })

        alert(this.state.eatingPattern)
    }


    render() {

        const { selectedOption } = this.state;
        const { selectedOptionGender } = this.state;
        const { open } = this.state;


        console.log("BMI: ", this.state.bmi)
        console.log("BMR: ", this.state.bmr)
        console.log("DER: ", this.state.DER)






        return (




            <div>

                <div>
                    <Jumbotron fluid>
                        <Container fluid>
                            <h3 className="display-3">Tell us more about what you like to eat..</h3>

                        </Container>
                    </Jumbotron>
                </div>


                <Container>
                    <Form>

                        <Row>
                            <Col md={6}>
                                <label><h4>Please select the food item you like the Most for each meal.</h4></label>
                                <FormGroup>

                                    <Label size="lg" for="exampleSelect"><img src="https://img.icons8.com/color/96/000000/breakfast.png" /> Breakfast </Label>
                                    <Input bsSize="lg" type="select" name="breakfastFood" id="breakfastFood" onChange={this.onChange} value={this.state.breakfastFood === null ? '' : this.state.breakfastFood} >
                                        <option data-image="icons/icon_calendar.gif">Rice</option>
                                        <option>Bread</option>
                                        <option>Rice flour based foods</option>
                                        <option>Wheat flour based foods </option>
                                        <option>Yams</option>
                                        <option>Grams</option>
                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                        <Row>
                            <Col md={6}>

                                <FormGroup>
                                    <Label size="lg" for="exampleSelect" > <img src="https://img.icons8.com/color/96/000000/lunch.png" /> Lunch</Label>
                                    <Input bsSize="lg" type="select" name="lunchFood" id="lunchFood" onChange={this.onChange} value={this.state.lunchFood === null ? '' : this.state.lunchFood} >
                                        <option>Rice</option>
                                        <option>Bread</option>
                                        <option>Wheat flour based foods </option>
                                        <option>Grams</option>
                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                        <Row>
                            <Col md={6}>

                                <FormGroup>
                                    <Label size="lg" for="exampleSelect"><img src="https://img.icons8.com/color/96/000000/dinner.png" /> Dinner </Label>
                                    <Input bsSize="lg" type="select" name="dinnerFood" id="dinnerFood" onChange={this.onChange} value={this.state.dinnerFood === null ? '' : this.state.dinnerFood} >
                                        <option>Rice</option>
                                        <option>Bread</option>
                                        <option>Rice flour based foods</option>
                                        <option>Wheat flour based foods </option>

                                    </Input>
                                </FormGroup>

                            </Col>

                        </Row>
                    </Form>
                </Container>
                <Container>
                    <Form>
                        {/* <Row> */}
                        {/* <Col md={6}> */}
                        {/* <FormGroup> */}
                        {/* <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/sneeze.png" />Test </Label> */}
                        {/* <div> */}
                        {/* <h1>Select Fruits</h1> */}
                        {/* <pre>{JSON.stringify(this.state.selected)}</pre> */}
                        {/* <MultiSelect */}
                        {/* options={optionsAllergies} */}
                        {/* value={this.state.selected} */}
                        {/* onChange={this.handleChangeMultiple} */}
                        {/* labelledBy={"Select"} */}
                        {/* /> */}
                        {/* </div> */}

                        {/* </FormGroup> */}
                        {/* </Col> */}
                        {/*  */}
                        {/* </Row> */}





                        <Row>
                            <br></br>
                            <br></br>
                            <Col md={6}>
                                <FormGroup>
                                    <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/sneeze.png" />Food Allergies & Intolerances </Label>
                                    {/* <div>
                                    <pre>{JSON.stringify(selected)}</pre>
                                    <MultiSelect
                                        options={optionsAllergies}
                                        value={selected}
                                        onChange={setSelected}
                                        labelledBy={"Select"}
                                    />
                                    </div> */}

                                    <MultiSelect
                                        options={optionsAllergies}
                                        value={this.state.selected}
                                        onChange={this.handleChangeMultiple}
                                        labelledBy={"Select"}
                                    />
                                    {/* <Input type="select" name="selectMulti" id="exampleSelectMulti" multiple onChange={this.handleChange} value={this.state.selectValue}>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input> */}
                                    {/* <Input type="text" name="address" id="exampleAddress" placeholder="" /> */}
                                </FormGroup>
                            </Col>

                        </Row>






                        <br></br>
                        <br></br>
                        <Row>

                            <Col md={6}>
                                <FormGroup>
                                    <Label size="lg" for="activityFactor"><img src="https://img.icons8.com/color/48/000000/health-book.png" />Please specify if you have any medical conditions</Label>
                                    {/* <Select bsSize="lg"
                                        value={this.state.selectedOptionMedical}
                                        onChange={this.handleChangeMedical}
                                        options={optionsMedical}
                                    /> */}
                                    <MultiSelect
                                        options={optionsMedical}
                                        value={this.state.selectedMed}
                                        onChange={this.handleChangeMultipleMed}
                                    // labelledBy={"Select"}
                                    />
                                    {/* <Input type="text" name="address" id="exampleAddress" placeholder="" /> */}
                                </FormGroup>
                            </Col>

                        </Row>
                        {/* <Row>
                            <Col md={12}>
                                <FormGroup>
                                    <Label size="lg" for="exampleSelect"> Please specify your eating pattern (Please refer the guide below) </Label>
                                    <Input bsSize="lg" type="select" name="eatingPattern" id="eatingPattern" onChange={this.onChange} value={this.state.eatingPattern === null ? '' : this.state.eatingPattern} >
                                        <option>Lacto-ovo-vegetarian</option>
                                        <option>Lacto-vegetarian</option>
                                        <option>Ovo-vegetarian</option>
                                        <option>Pesco-vegetarian</option>
                                        <option>Semi-vegetarian</option>
                                        <option>Vegan</option>
                                        <option>I eat Anything</option>
                                    </Input>
                                </FormGroup>
                            </Col>

                        </Row> */}


                        <Col md={12}>
                            <br></br>
                            <br></br>
                            <Label size="lg" for="exampleAddress">Please Select Your Eating Pattern</Label>
                            <br></br>
                            <br></br>
                            <Table hover>
                                <thead>
                                    <tr>

                                        <th>Type</th>
                                        <th>Description</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr onClick={() => this.handleTableSelect("Lacto-ovo-vegetarian")} style={{ backgroundColor: this.state.eatingPattern === "Lacto-ovo-vegetarian" ? 'lightgrey' : null }}>

                                        <td><h6><b>Lacto-ovo-vegetarians </b></h6></td>
                                        <td> If you eat plant-based foods, dairy products, and eggs, and exclude meat, poultry, and fish. <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /></td>

                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("Lacto-vegetarians")} style={{ backgroundColor: this.state.eatingPattern === "Lacto-vegetarians" ? 'lightgrey' : null }}>

                                        <td><h6><b>Lacto-vegetarians</b></h6></td>
                                        <td>If you eat plant-based foods and dairy products, and exclude meat, poultry, fish, and eggs. <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /><img src="https://img.icons8.com/color/48/000000/no-eggs.png" /></td>
                                        <td></td>

                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("Ovo-vegetarians")} style={{ backgroundColor: this.state.eatingPattern === "Ovo-vegetarians" ? 'lightgrey' : null }}>

                                        <td><h6><b>Ovo-vegetarians </b></h6></td>
                                        <td>If you eat plant-based foods and eggs, and exclude meat, poultry, fish, and dairy products. <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-fish.png" /><img src="https://img.icons8.com/color/48/000000/no-milk.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("Pesco-vegetarians")} style={{ backgroundColor: this.state.eatingPattern === "Pesco-vegetarians" ? 'lightgrey' : null }}>

                                        <td><h6><b>Pesco-vegetarians </b></h6></td>
                                        <td>If you eat a vegetarian diet but also include fish. <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/fish-food.png" /><img src="https://img.icons8.com/color/48/000000/no-milk.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /><img src="https://img.icons8.com/color/48/000000/no-eggs.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("Semi-vegetarians")} style={{ backgroundColor: this.state.eatingPattern === "Semi-vegetarians" ? 'lightgrey' : null }}>

                                        <td><h6><b>Semi-vegetarians </b></h6></td>
                                        <td>You may eat dairy products or eggs, as well as a little fish and chicken, and generally exclude meat; <br /> <img src="https://img.icons8.com/color/48/000000/salad.png" /><img src="https://img.icons8.com/color/48/000000/milk-bottle.png" /><img src="https://img.icons8.com/color/48/000000/sunny-side-up-eggs.png" /><img src="https://img.icons8.com/color/48/000000/fish-food.png" /><img src="https://img.icons8.com/color/48/000000/thanksgiving.png" /><img src="https://img.icons8.com/color/48/000000/no-meat.png" /></td>
                                        <td></td>
                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("Vegans")} style={{ backgroundColor: this.state.eatingPattern === "Vegans" ? 'lightgrey' : null }}>

                                        <td><h6><b>Vegans</b></h6></td>
                                        <td>If you eat plant-based foods only, excluding all foods of animal origin <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /></td>

                                    </tr>
                                    <tr onClick={() => this.handleTableSelect("I eat Anything")} style={{ backgroundColor: this.state.eatingPattern === "I eat Anything" ? 'lightgrey' : null }}>

                                        <td><h6><b>I eat Anything</b></h6></td>
                                        <td>I eat Anything <br /><img src="https://img.icons8.com/color/48/000000/salad.png" /></td>

                                    </tr>
                                </tbody>
                            </Table>
                        </Col>


                    </Form>





                    <div>

                        <Button size="lg" color="success" onClick={() => this.saveUserInfo()}>Generate Meal</Button>
                    </div>



                </Container>






            </div>

        )
    }
}

export default UserPreference